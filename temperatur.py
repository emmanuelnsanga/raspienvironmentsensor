#!/usr/bin/python

import Adafruit_DHT
import time

sensor = Adafruit_DHT.DHT22

pin = 4

while True:


	humidity, temperature= Adafruit_DHT.read_retry(sensor,pin)

	if humidity is not None and temperature is not None:
		print("Temperatur = {0:0.1f} C       Luftfeuchtigkeit = {1:0.1f}%".format(temperature,humidity))

	else:
		print("Error")
	
	time.sleep(10)
