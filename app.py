from flask import Flask, render_template, request, jsonify, flash, redirect, url_for
from werkzeug.utils import secure_filename
from flask_socketio import SocketIO, emit
import os
import time
import pandas as pd
import json
from datetime import date
from apscheduler.schedulers.background import BackgroundScheduler
import atexit
#import serial
#from light import readLight
#import Adafruit_DHT
#from particles import measure_particles

### Global Variables ###

# Temperature and Humidity
temperature_gpio = 4
#sensor = Adafruit_DHT.DHT22

# Sound
noice_counter = 0

# Web server and sockets
app = Flask(__name__)
socketio = SocketIO(app, cors_allowed_origins="*")

# Definition of web sockets
@socketio.on('connect')
def handle_connect():
    print('Server: Client connected')

@socketio.on('noisy')
def noice_detected():
    print('Noice detected!')
    noice_counter =+1

# Sensor Method
def read_all_sensors():

    light = 300
    humidity = 50
    temperature = 20
    pm25 = 5
    pm10 = 10
    co2 = 400

    #light=readLight()
    #humidity, temperature= Adafruit_DHT.read_retry(sensor,temperature_gpio)
    #pm25, pm10 = measure_particles()
    #co2 = 400


    socketio.emit('send_data', {'light': light, 'temperatur':temperature, 'humidity':humidity, 'co2':co2, 'pm2':pm25, 'pm10':pm10})


# Scheduling sensors
scheduler = BackgroundScheduler()
scheduler.add_job(func=read_all_sensors, trigger="interval", seconds=60)
scheduler.start()

# Shut down the scheduler when exiting the app
atexit.register(lambda: cron.shutdown(wait=False))



@app.route('/')
def hello():
    
    return render_template('index.html')


if __name__ == "__main__":
    app.run(debug=False)