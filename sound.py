#!/usr/bin/python

import RPi.GPIO as GPIO
import time
import socketio

# asyncio
sio = socketio.AsyncClient()

sio.connect('http://localhost:5000')

#GPIO definieren
channel = 17
GPIO.setmode(GPIO.BCM)
GPIO.setup(channel, GPIO.IN)

# Immer wenn sich Signal am Pin aendert dem Server mitteilen
def callback(channel):
	if GPIO.input(channel):
		sio.emit('noisy')
	else:
		sio.emit('noisy')

GPIO.add_event_detect(channel, GPIO.BOTH, bouncetime=600)
GPIO.add_event_callback(channel, callback)

while True:
	time.sleep(5)