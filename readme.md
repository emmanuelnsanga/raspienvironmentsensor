# Environment Sensor

Raspberry Pi project with various sensors to measure environmental parameters to ensure a good working environment and maintain a high level of prodoctivity.

![Sketch](Umgebungssensor_PWM_Steckplatine.png?raw=true "Sketch")

## Measurements

- Temperature
- Humidity
- Noise
- Brightness (LUX)
- PM2.5 and PM10
- CO2

